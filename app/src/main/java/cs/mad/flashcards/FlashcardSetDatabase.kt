package cs.mad.flashcards

import androidx.room.Database
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao

@Database(entities=[FlashcardSet::class], version=1)
abstract class FlashcardSetDatabase: RoomDatabase() {
    companion object{
        const val databaseName = "FLASHCARDSET_DATABASE"
    }

    abstract fun flashDao() : FlashcardSetDao
}