package cs.mad.flashcards.entities

import androidx.room.*


@Entity
data class Flashcard(
    val question: String,
    val answer: String
) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Def $i"))
            }
            return cards
        }
    }
}



@Dao
interface FlashcardDao{
    @Query("select * from Flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(fc: Flashcard)

    @Insert
    suspend fun insertAll(fcs: List<Flashcard>)

    @Update
    suspend fun update(fc: Flashcard)

    @Delete
    suspend fun delete(fc: Flashcard)

    @Query("delete from Flashcard")
    suspend fun deleteAll()
}