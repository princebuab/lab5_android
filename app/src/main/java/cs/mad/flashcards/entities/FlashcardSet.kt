package cs.mad.flashcards.entities

import androidx.room.*

//data class FlashcardSetContainer(
//    val flashcardSet: List<FlashcardSet>
//)

@Entity
data class FlashcardSet(
    val title: String,
    @PrimaryKey val id: Long? = null
) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet("Set $i"))
            }
            return sets
        }
    }
}

@Dao
interface FlashcardSetDao {
    @Query("select * from FlashcardSet")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(fs: FlashcardSet)

    @Insert
    suspend fun insertAll(fss: List<FlashcardSet>)

    @Update
    suspend fun update(fs: FlashcardSet)

    @Delete
    suspend fun delete(fs: FlashcardSet)

    @Query("delete from FlashcardSet")
    suspend fun deleteAll()
}